<?php
$name = $email = $phone = $message = $mailCheck = "";
$address = $subject = $content = $headers = "";

if(isset($_POST["one"])) { $name = $_POST["one"]; }
if(isset($_POST["two"])) { $email = $_POST["two"]; }
if(isset($_POST["three"])) { $phone = $_POST["three"]; }
if(isset($_POST["four"])) { $message = $_POST["four"]; }
$message = wordwrap($message,70);

function feedbackForm($data) 
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

$name = feedbackForm($_POST["one"]);
$email = feedbackForm($_POST["two"]);
$phone = feedbackForm($_POST["three"]);
$message = feedbackForm($_POST["four"]);

function antiSpam($field) 
{
   $field = filter_var($field,FILTER_SANITIZE_EMAIL);
   if(filter_var($field,FILTER_VALIDATE_EMAIL)) { return true; }
   else { return false; }
}

$mailCheck = antiSpam($_POST["two"]);

$address = "atlanticdent@gmail.com";
$subject = "Новый клиент";
$content = 
"
<div><strong>Имя:</strong> $name</div><br />
<div><strong>E-mail:</strong> $email</div><br />
<div><strong>Номер мобильного телефона:</strong> $phone</div><br />
<div><strong>Содержание письма:</strong> $message</div>
";
$headers = "MIME-Version: 1.0" . "\r\n" . "content-type: text/html; charset=UTF-8" . "\r\n" . "From: $email";

mail($address,$subject,$content,$headers);
?>
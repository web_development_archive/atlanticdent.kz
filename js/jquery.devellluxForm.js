$(document).ready(function() {
var form = $("#feedback_form"),
name = $("#feedback_form input[name='one']"),
email = $("#feedback_form input[name='two']"),
phone = $("#feedback_form input[name='three']"),
message = $("#feedback_form textarea[name='four']"),
button = $("#feedback_form button[name='submit']"),
result = $("#feedback_form #result"),
strong = $("#feedback_form strong"),
inputTextarea = $("#feedback_form input, textarea"),
antiBot = $("#feedback_form input[name='email']");

name.bind("change keyup",function() {
var nameRegular = /^[а-яА-Яa-zA-Z ]*$/;
var nameLength = name.val().length;
if(name.val() != "" && nameRegular.test(name.val()) && nameLength >= 3) {
name.css("border","1px solid green");
name.next().html("&#10003;").css("color","green");
} else {
if(!nameRegular.test(name.val())) {
name.css("border","1px solid red");
name.next().text("Допустимы только буквы.").css("color","red");
} else if(nameLength <= 2) {
name.css("border","1px solid red");
name.next().text("Минимальное количество букв 3.").css("color","red");
}
if(name.val() == "") {
name.css("border","1px solid red");
name.next().text("Обязательное поле.").css("color","red");
}
}
});

email.bind("change keyup",function() {
var emailRegular = /([\w\-]+\@[\w\-]+\.[\w\-]+[\w\-]+)/;
var spamRegular = /([\w\-]+\@[\w\-]+\.[\w\-]+[\w\-]+[`\~\!\@\"\'\#\№\$\;\:\%\^\&\?\*\(\)\-\_\+\=\{\}\[\]\|\\\/\.\,\<\>]+)/;
if(email.val() != "" && emailRegular.test(email.val()) && !spamRegular.test(email.val())) {
email.css("border","1px solid green");
email.next().html("&#10003;").css("color","green");
} else {
if(!emailRegular.test(email.val())) {
email.css("border","1px solid red");
email.next().text("Не правильный адрес электронной почты. Пример: example@gmail.com").css("color","red");
} else if(spamRegular.test(email.val())) {
email.css("border","1px solid red");
email.next().text("Пожалуйста, уберите лишние символы. Включён анти-спам!").css("color","red");
}
if(email.val() == "") {
email.css("border","1px solid red");
email.next().text("Обязательное поле.").css("color","red");
}
}
});

phone.bind("change keyup",function() {
var phoneRegular = /^[0-9+\-\(\) ]*$/;
var phoneLength = phone.val().length;
if(phone.val() != "" && phoneRegular.test(phone.val()) && phoneLength >= 7) {
phone.css("border","1px solid green");
phone.next().html("&#10003;").css("color","green");
} else {
if(!phoneRegular.test(phone.val())) {
phone.css("border","1px solid red");
phone.next().text("Допустимы только цифры. Пример: +7(123) 456 78 90").css("color","red");
} else if(phoneLength <= 6) {
phone.css("border","1px solid red");
phone.next().text("Минимальное количество цифр 7.").css("color","red");
}
if(phone.val() == "") {
phone.css("border","1px solid red");
phone.next().text("Обязательное поле.").css("color","red");
}
}
});

message.bind("change keyup",function() {
var messageRegular = /^[0-9а-яА-Яa-zA-Z.\,\!\?\(\)\-\+\_\:\=\@ ]*$/;
var messageLength = message.val().length;
if(message.val() != "" && messageRegular.test(message.val()) && messageLength >= 20) {
message.css("border","1px solid green");
message.next().html("&#10003;").css("color","green");
} else {
if(!messageRegular.test(message.val())) {
message.css("border","1px solid red");
message.next().text("Допустимы только буквы, цифры, пробелы и следующие символы: . , ! ? ( ) - + _ : = @").css("color","red");
} else if(messageLength <= 19) {
message.css("border","1px solid red");
message.next().text("Минимальное количество букв 20.").css("color","red");
}
if(message.val() == "") {
message.css("border","1px solid red");
message.next().text("Обязательное поле.").css("color","red");
}
}
});

form.submit(function(event) {
var nameRegular = /^[а-яА-Яa-zA-Z ]*$/;
var nameLength = name.val().length;
if(name.val() != "" && nameRegular.test(name.val()) && nameLength >= 3) {
name.css("border","1px solid green");
name.next().html("&#10003;").css("color","green");
} else {
if(!nameRegular.test(name.val())) {
name.css("border","1px solid red");
name.next().text("Допустимы только буквы.").css("color","red");
} else if(nameLength <= 2) {
name.css("border","1px solid red");
name.next().text("Минимальное количество букв 3.").css("color","red");
}
if(name.val() == "") {
name.css("border","1px solid red");
name.next().text("Обязательное поле.").css("color","red");
}
}

var emailRegular = /([\w\-]+\@[\w\-]+\.[\w\-]+[\w\-]+)/;
var spamRegular = /([\w\-]+\@[\w\-]+\.[\w\-]+[\w\-]+[`\~\!\@\"\'\#\№\$\;\:\%\^\&\?\*\(\)\-\_\+\=\{\}\[\]\|\\\/\.\,\<\>]+)/;
if(email.val() != "" && emailRegular.test(email.val()) && !spamRegular.test(email.val())) {
email.css("border","1px solid green");
email.next().html("&#10003;").css("color","green");
} else {
if(!emailRegular.test(email.val())) {
email.css("border","1px solid red");
email.next().text("Не правильный адрес электронной почты. Пример: example@gmail.com").css("color","red");
} else if(spamRegular.test(email.val())) {
email.css("border","1px solid red");
email.next().text("Пожалуйста, уберите лишние символы. Включён анти-спам!").css("color","red");
}
if(email.val() == "") {
email.css("border","1px solid red");
email.next().text("Обязательное поле.").css("color","red");
}
}

var phoneRegular = /^[0-9+\-\(\) ]*$/;
var phoneLength = phone.val().length;
if(phone.val() != "" && phoneRegular.test(phone.val()) && phoneLength >= 7) {
phone.css("border","1px solid green");
phone.next().html("&#10003;").css("color","green");
} else {
if(!phoneRegular.test(phone.val())) {
phone.css("border","1px solid red");
phone.next().text("Допустимы только цифры. Пример: +7(123) 456 78 90").css("color","red");
} else if(phoneLength <= 6) {
phone.css("border","1px solid red");
phone.next().text("Минимальное количество цифр 7.").css("color","red");
}
if(phone.val() == "") {
phone.css("border","1px solid red");
phone.next().text("Обязательное поле.").css("color","red");
}
}

var messageRegular = /^[0-9а-яА-Яa-zA-Z.\,\!\?\(\)\-\+\_\:\=\@ ]*$/;
var messageLength = message.val().length;
if(message.val() != "" && messageRegular.test(message.val()) && messageLength >= 20) {
message.css("border","1px solid green");
message.next().html("&#10003;").css("color","green");
} else {
if(!messageRegular.test(message.val())) {
message.css("border","1px solid red");
message.next().text("Допустимы только буквы, цифры, пробелы и следующие символы: . , ! ? ( ) - + _ : = @").css("color","red");
} else if(messageLength <= 19) {
message.css("border","1px solid red");
message.next().text("Минимальное количество букв 20.").css("color","red");
}
if(message.val() == "") {
message.css("border","1px solid red");
message.next().text("Обязательное поле.").css("color","red");
}
}

if(
name.val() != "" && nameRegular.test(name.val()) && nameLength >= 3 &&
email.val() != "" && emailRegular.test(email.val()) && !spamRegular.test(email.val()) &&
phone.val() != "" && phoneRegular.test(phone.val()) && phoneLength >= 7 &&
message.val() != "" && messageRegular.test(message.val()) && messageLength >= 20 && antiBot.val() == ""
) {
$.ajax({
type: "POST",
url: "handler.php",
data: form.serialize(),
dataType: "html",
success: function() {
$(".plane").addClass("takeoff_animation");
result.html("&#10003; Ваше сообщение успешно отправлено.").css("color","green").fadeOut(5000);
form.trigger("reset");
strong.empty();
inputTextarea.css({
"border":"1px solid #DCDCDC",
"-moz-transition":"all 2.5s linear",
"-webkit-transition":"all 2.5s linear",
"-o-transition":"all 2.5s linear",
"-ms-transition":"all 2.5s linear",
"transition":"all 2.5s linear"
});
setTimeout(function() { location.reload(); },5500);
},
error: function() {
$(".plane").addClass("not_takeoff_animation");
result.html("&#10007; Ваше сообщение не отправлено, произошла ошибка на сервере. Попробуйте ещё раз.").css("color","red").fadeOut(5000);
form.trigger("reset");
strong.empty();
inputTextarea.css({
"border":"1px solid #DCDCDC",
"-moz-transition":"all 2.5s linear",
"-webkit-transition":"all 2.5s linear",
"-o-transition":"all 2.5s linear",
"-ms-transition":"all 2.5s linear",
"transition":"all 2.5s linear"
});
setTimeout(function() { location.reload(); },5500);
}
});
} else { return false; }
event.preventDefault();
});
});
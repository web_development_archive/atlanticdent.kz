function ourLocation() {
var ourLatLng = new google.maps.LatLng(43.319933,76.939289,true);
var mapOptions = {
center: ourLatLng, 
zoom: 15, 
mapTypeId: google.maps.MapTypeId.ROADMAP,
mapTypeControl: true, 
mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU },
panControl: true, 
panControlOptions: { position: google.maps.ControlPosition.TOP_LEFT }
};
var map = new google.maps.Map(document.getElementById("our_location"), mapOptions);
var panoramioLayer = new google.maps.panoramio.PanoramioLayer({ map: map });
var icon = "favicon.png";
var marker = new google.maps.Marker({ map: map, position: ourLatLng, animation: google.maps.Animation.DROP, icon: icon, title: "Наш офис" });
var infoWindowСontent = '<img src="images/logotype.png" style="    width: 50px;    float: left;    margin: 0 10px 0 0;    " >Казахстан, г. Алматы,' + "<br />" + "ул. Сейфуллина, 498," + "<br />" + "уг. ул. Богенбай батыра, офис 208";
var infoWindow = new google.maps.InfoWindow({ map: map, position: ourLatLng, content: infoWindowСontent });
var panorama = new google.maps.StreetViewPanorama(document.getElementById("our_location"), { position: position, enableCloseButton: true });
map.setStreetView(panorama);
var mapStyles = [ { featureType: "all", elementType: "all", stylers: [ { visibility: "on" } ] } ];
map.setOptions({ styles: mapStyles });
google.maps.event.addListener(marker, "click", function() { infoWindow.open(map,marker); });
}
google.maps.event.addDomListener(window, "load", ourLocation);
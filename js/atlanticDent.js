/* :: atlanticDent.js
-----------------------------------------------------------------------------*/
$(document).ready(function() {

    $(".doc_1").click(function(){

        $(".Sergey").fadeOut();
        $(".Asiya").fadeOut();
        $(".Kiril").fadeOut();
        $(".beckzhan").fadeIn();
    });
    $(".doc_3").click(function(){
        $(".beckzhan").fadeOut();

        $(".Asiya").fadeOut();
        $(".Kiril").fadeOut();
        $(".Sergey").fadeIn();
    });
    $(".doc_2").click(function(){
        $(".beckzhan").fadeOut();
        $(".Sergey").fadeOut();

        $(".Kiril").fadeOut();
        $(".Asiya").fadeIn();
    });
    $(".doc_4").click(function(){
        $(".beckzhan").fadeOut();
        $(".Sergey").fadeOut();
        $(".Asiya").fadeOut();
        $(".Kiril").fadeIn();
    });

    $("body").append('<div class="button-up" style="display: none; z-index: 9; padding: 250px 0;opacity: 0.7;width: 50px;height:100%;position: fixed;right: 0px;top: 0px;cursor: pointer;text-align: center;line-height: 30px;color: #d3dbe4;font-weight: bold;"><img src="images/arrowblack.png" alt></div>');
    $('.button-up').click(function(){
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $("#main_menu ul li a").click(function(event) {
$("html,body").stop().animate({ scrollTop: $($(this).attr("href")).offset().top - 35 }, 1500, "easeInOutExpo");
return false;
event.preventDefault();
});

$(window).scroll(function() {
    if ($ (this).scrollTop () > 100) {
        $ ('.button-up').fadeIn();
    } else {
        $ ('.button-up').fadeOut();
    }
var scroll = $(this).scrollTop();
if(scroll >= 35) { $("#main_menu").addClass("fixed"); } 
else { $("#main_menu").removeClass("fixed"); }
$("#main_menu ul li a").each(function() {
if(scroll >= $($(this).attr("href")).offset().top - 40) {
$("#main_menu ul li a").not(this).removeClass("active");
$(this).addClass("active");
}
});
});

if($(this).scrollTop() <= 585) { $("#main_menu ul li a[href='#header']").addClass("active"); } 
else { $("#main_menu ul li a[href='#header']").removeClass("active"); }

var ms = ss = mm = hh = 0;
setInterval(function() {
ms += 9;
if(ms == 999) { ms = 0; ss += 1; }
if(ss == 60) { ss = 0; mm += 1; }
if(mm == 60) { mm = 0; hh += 1; }
$(".stopwatch").html(
("0" + hh).slice(-2) + " : " + 
("0" + mm).slice(-2) + " : " + 
("0" + ss).slice(-2) + " : " + 
("00" + ms).slice(-3));
}, 8);

$(".callback").fancybox({
width: 300,
height: 300,
openSpeed: 500,
closeSpeed: 500,
autoSize: false
});

$(".news").fancybox({
width: 500,
height: 500,
openSpeed: 500,
closeSpeed: 500,
autoSize: false
});

$(".doctor").fancybox({
    width: 960,
    height: 900,
    openSpeed: 500,
    closeSpeed: 500,
    autoSize: false
});

    $(".feedback").fancybox({
width: 900,
height: 340,
openSpeed: 500,
closeSpeed: 500,
autoSize: false
});

$("#slider_column").bxSlider({
mode: "fade",
speed: 2500,
controls: false,
auto: true
});

$("#reviews_slider_column").bxSlider({
mode: "horizontal",
speed: 500,
pause: 10000,
controls: false,
auto: true
});

$("#qn_ar_column dl dd").hide();
$("#qn_ar_column dl dt").click(function(event) {
$(this).next().slideToggle(1000, "easeOutBounce");
$(this).toggleClass("active dt_b_r");
return false;
event.preventDefault();
});

$(".tab_content").addClass("fadeIn").not(":first").hide();
$(".tabs_links ul li a:first").addClass("active");
$(".tabs_links ul li a").click(function(event) {
$(".tabs_links ul li a").removeClass("active");
$(this).addClass("active");
$(".tab_content").hide();
$($(this).attr("href")).fadeIn();
return false;
event.preventDefault();
});

var page = 2;

var loadingContent = function(result) {
$("#ajax_status").hide();
$("#load_more").show();
page += 1;
$(result).appendTo("#news_data_column");
};

$("#load_more").click(function(event) {
var url = "news/load_more/" + page + ".html";
$(this).hide();
$("#ajax_status").show();

setTimeout(function() {
$.ajax({
url: url,
success: function(result) {
if(!result || result.trim() == "none") {
$("#load_more").fadeOut();
$("#ajax_status").text("Все новости загружены!").addClass("all_loaded");
return;
}
loadingContent(result);
},
error: function(result) {
$("#ajax_status").text("Сервер или содержимое временно не доступны! Пожалуйста, повторите попытку позже.");
}
});
}, 3000);
return false;
event.preventDefault();
});

});